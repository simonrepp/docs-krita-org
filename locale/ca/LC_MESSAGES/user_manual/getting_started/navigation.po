# Translation of docs_krita_org_user_manual___getting_started___navigation.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 17:10+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: clic dret del ratolí"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: clic del mig del ratolí"

#: ../../user_manual/getting_started/navigation.rst:None
msgid ".. image:: images/Interface-tour.svg"
msgstr ".. image:: images/Interface-tour.svg"

#: ../../user_manual/getting_started/navigation.rst:None
msgid ".. image:: images/Krita-popuppalette.png"
msgstr ".. image:: images/Krita-popuppalette.png"

#: ../../user_manual/getting_started/navigation.rst:1
msgid "Overview of Krita navigation."
msgstr "Visió general de la navegació al Krita."

#: ../../user_manual/getting_started/navigation.rst:13
#: ../../user_manual/getting_started/navigation.rst:18
#: ../../user_manual/getting_started/navigation.rst:43
msgid "Navigation"
msgstr "Navegació"

#: ../../user_manual/getting_started/navigation.rst:13
#: ../../user_manual/getting_started/navigation.rst:103
msgid "Pop-up Palette"
msgstr "Paleta emergent"

#: ../../user_manual/getting_started/navigation.rst:13
msgid "Zoom"
msgstr "Fer «zoom»"

#: ../../user_manual/getting_started/navigation.rst:13
msgid "Rotate"
msgstr "Girar"

#: ../../user_manual/getting_started/navigation.rst:13
msgid "Pan"
msgstr "Desplaçar"

#: ../../user_manual/getting_started/navigation.rst:13
msgid "Workspace"
msgstr "Espai de treball"

#: ../../user_manual/getting_started/navigation.rst:21
msgid "Interface"
msgstr "Interfície"

#: ../../user_manual/getting_started/navigation.rst:23
msgid ""
"Krita's interface is very flexible and provides an ample choice for the "
"artists to arrange the elements of the workspace. An artist can snap and "
"arrange the elements, much like snapping together Lego blocks. Krita "
"provides a set of construction kit parts in the form of Dockers and "
"Toolbars. Every set of elements can be shown, hidden, moved and rearranged "
"that let the artists easily customize their own user interface experience."
msgstr ""
"La interfície del Krita és molt flexible i ofereix una àmplia selecció per a "
"que els artistes organitzin els elements de l'espai de treball. Un artista "
"pot ajustar i organitzar els elements, com quan s'encaixen blocs de Lego. El "
"Krita proporciona un conjunt de parts del kit de construcció en forma "
"d'Acobladors i Barres d'eines. Cada conjunt d'elements es pot mostrar, "
"ocultar, moure i reorganitzar per a permetre als artistes personalitzar amb "
"facilitat la seva pròpia experiència en la interfície d'usuari."

#: ../../user_manual/getting_started/navigation.rst:26
msgid "A Tour of the Krita Interface"
msgstr "Una gira per la interfície del Krita"

#: ../../user_manual/getting_started/navigation.rst:28
msgid ""
"As we've said before, the Krita interface is very malleable and the way that "
"you choose to configure the work surface may not resemble those shown below, "
"but we can use these as a starting point."
msgstr ""
"Com ja hem dit anteriorment, la interfície del Krita és molt manejable i la "
"manera que trieu de configurar la superfície de treball pot no semblar-se a "
"la que es mostra a continuació, però podem utilitzar-la com a punt de "
"partida."

#: ../../user_manual/getting_started/navigation.rst:34
msgid ""
"**A** -- Traditional :guilabel:`File` or action menu found in most windowed "
"applications."
msgstr ""
"**A** -- El menú :guilabel:`Fitxer` o acció tradicional es troba en la "
"majoria de les aplicacions amb finestra."

#: ../../user_manual/getting_started/navigation.rst:35
msgid ""
"**B** -- Toolbar - This is where you can choose your brushes, set parameters "
"such as opacity and size and other settings."
msgstr ""
"**B** -- Barra d'eines: aquí podreu escollir els pinzells, establir els "
"paràmetres com l'opacitat, la mida i altres ajustaments."

#: ../../user_manual/getting_started/navigation.rst:36
msgid ""
"**C** -- Sidebars for the Movable Panels/Dockers. In some applications, "
"these are known as Dockable areas. Krita also allows you to dock panels at "
"the top and/or bottom as well."
msgstr ""
"**C** -- Barres laterals per als Plafons/Acobladors mòbils. En algunes "
"aplicacions, aquestes es coneixen com a àrees acoblables. El Krita també "
"permet acoblar els plafons a la part superior i/o inferior."

#: ../../user_manual/getting_started/navigation.rst:37
msgid ""
"**D** -- Status Bar - This space shows the preferred mode for showing "
"selection i.e. marching ants or mask mode, your selected brush preset, :ref:"
"`Color Space <color_managed_workflow>`, image size and provides a convenient "
"zoom control."
msgstr ""
"**D** -- Barra d'estat: aquest espai mostra el vostre mode preferit mostrant "
"el seleccionat, p. ex., els modes formigues o màscara, el pinzell predefinit "
"seleccionat, l':ref:`Espai de color <color_managed_workflow>`, mida de la "
"imatge i proporciona un còmode control del zoom."

#: ../../user_manual/getting_started/navigation.rst:38
msgid ""
"**E** -- Floating Panel/Docker - These can be \"popped\" in and out of their "
"docks at any time in order to see a greater range of options. A good example "
"of this would be the :ref:`brush_preset_docker` or the :ref:`palette_docker`."
msgstr ""
"**E** -- Plafó/Acoblador flotant: aquests poden «obrir-se» dins i fora dels "
"seus acobladors en qualsevol moment per a veure un ampli ventall de les "
"opcions. Un bon exemple d'això seria l':ref:`brush_preset_docker` o l':ref:"
"`palette_docker`."

#: ../../user_manual/getting_started/navigation.rst:40
msgid ""
"Your canvas sits in the middle and unlike traditional paper or even most "
"digital painting applications, Krita provides the artist with a scrolling "
"canvas of infinite size (not that you'll need it of course!). The standard "
"navigation tools are as follows:"
msgstr ""
"El vostre llenç es troba al mig i, a diferència del paper tradicional o fins "
"i tot de la majoria de les aplicacions de pintura digital, el Krita "
"proporciona a l'artista un llenç desplaçable de mida infinita (per "
"descomptat, no ho necessitareu!). Les eines de navegació estàndard són les "
"següents:"

#: ../../user_manual/getting_started/navigation.rst:44
msgid ""
"Many of the canvas navigation actions, like rotation, mirroring and zooming "
"have default keys attached to them:"
msgstr ""
"Moltes de les accions de navegació del llenç, com ara el gir, el mirall i el "
"zoom, tenen dreceres predeterminades relacionades amb elles:"

#: ../../user_manual/getting_started/navigation.rst:46
msgid "Panning"
msgstr "Desplaçar"

#: ../../user_manual/getting_started/navigation.rst:47
msgid ""
"This can be done through |mousemiddle|, :kbd:`Space +` |mouseleft| and :kbd:"
"`the directional keys`."
msgstr ""
"Es pot fer mitjançant el |mousemiddle|, :kbd:`Espai + feu` |mouseleft| i :"
"kbd:`les tecles direccionals`."

#: ../../user_manual/getting_started/navigation.rst:48
msgid "Zooming"
msgstr "Fer «zoom»"

#: ../../user_manual/getting_started/navigation.rst:49
msgid ""
"Discrete zooming can be done through :kbd:`+` and :kbd:`-` keys. Using the :"
"kbd:`Ctrl + Space` or :kbd:`Ctrl +` |mousemiddle| shortcuts can allow for "
"direct zooming with the stylus."
msgstr ""
"Es pot fer zoom discret mitjançant les tecles :kbd:`+`, and :kbd:`-`. "
"Emprant les dreceres :kbd:`Ctrl + Espai` o :kbd:`Ctrl + feu` |mousemiddle| "
"es permet fer zoom directe amb el llapis."

#: ../../user_manual/getting_started/navigation.rst:50
msgid "Mirroring"
msgstr "Emmirallar"

#: ../../user_manual/getting_started/navigation.rst:51
msgid ""
"You can mirror the view can be quickly done via :kbd:`M` key. Mirroring is a "
"great technique that seasoned digital artists use to quickly review the "
"composition of their work to ensure that it \"reads\" well, even when "
"flipped horizontally."
msgstr ""
"Podeu emmirallar la vista amb rapidesa mitjançant la tecla :kbd:`M`. "
"Emmirallar és una gran tècnica que els artistes digitals amb experiència fan "
"servir per a revisar ràpidament la composició del seu treball per assegurar-"
"se que es «vegi» bé, fins i tot quan es volteja horitzontalment."

#: ../../user_manual/getting_started/navigation.rst:53
msgid "Rotating"
msgstr "Girar"

#: ../../user_manual/getting_started/navigation.rst:53
msgid ""
"You can rotate the canvas without transforming. It can be done with the :kbd:"
"`Ctrl + [` shortcut or :kbd:`4`key  and the other way with :kbd:`Ctrl + ]` "
"shortcut or :kbd:`6` key. Quick mouse based rotation is the :kbd:`Shift + "
"Space` and :kbd:`Shift +` |mousemiddle| shortcuts. To reset rotation use "
"the :kbd:`5` key."
msgstr ""
"Podeu girar el llenç sense transformar-lo. Es pot fer mitjançant la drecera :"
"kbd:`Ctrl + [` o :kbd:`4` i l'altre manera amb la drecera :kbd:`Ctrl + ]` o "
"la tecla :kbd:`6`. El gir ràpid basat en el ratolí és amb les dreceres :kbd:"
"`Majús. + Espai` i :kbd:`Majús. +` clic |mousemiddle|. Per a reiniciar el "
"gir empreu la tecla :kbd:`5`."

#: ../../user_manual/getting_started/navigation.rst:55
msgid "You can also find these under :menuselection:`View --> Canvas`."
msgstr "També les trobareu sota :menuselection:`Visualitza --> Llenç`."

#: ../../user_manual/getting_started/navigation.rst:58
msgid "Dockers"
msgstr "Acobladors"

#: ../../user_manual/getting_started/navigation.rst:60
msgid ""
"Krita subdivides many of its options into functional panels called Dockers "
"(also known as Docks)."
msgstr ""
"El Krita subdivideix moltes de les seves opcions en plafons funcionals "
"anomenats Acobladors."

#: ../../user_manual/getting_started/navigation.rst:62
msgid ""
"Dockers are small windows that can contain, for example, things like the "
"layer stack, Color Palette or list of Brush Presets. Think of them as the "
"painter's palette, or his water, or his brush kit. They can be activated by "
"choosing the :guilabel:`Settings` menu and the :guilabel:`Dockers` sub-menu. "
"There you will find a long list of available options."
msgstr ""
"Els acobladors són petites finestres que poden contenir, per exemple, coses "
"com la pila de capes, la Paleta de colors o la llista dels Pinzells "
"predefinits. Penseu-hi com la paleta d'un pintor, en la seva aigua o en el "
"seu pinzell. Es poden activar escollint el menú :guilabel:`Arranjament` i el "
"submenú :guilabel:`Acobladors`. Hi trobareu una llarga llista d'opcions "
"disponibles."

#: ../../user_manual/getting_started/navigation.rst:64
msgid ""
"Dockers can be removed by clicking the **x** in the upper-right of the "
"docker-window."
msgstr ""
"Els acobladors es poden eliminar fent clic a la **x** que es troba a la part "
"superior dreta de la finestra acoblada."

#: ../../user_manual/getting_started/navigation.rst:66
msgid ""
"Dockers, as the name implies, can be docked into the main interface. You can "
"do this by dragging the docker to the sides of the canvas (or top or bottom "
"if you prefer)."
msgstr ""
"Els acobladors, com el seu nom indica, es poden acoblar a la interfície "
"principal. Podeu fer-ho arrossegant l'acoblador cap als costats del llenç (o "
"la part superior o inferior si ho preferiu)."

#: ../../user_manual/getting_started/navigation.rst:68
msgid ""
"Dockers contain many of the \"hidden\", and powerful, aspects of **Krita** "
"that you will want to explore as you start delving deeper into the "
"application."
msgstr ""
"Els acobladors contenen molts dels aspectes «ocults» i potents del **Krita** "
"que voldreu explorar a mesura que comenceu a aprofundir en l'aplicació."

# skip-rule: kct-docker
#: ../../user_manual/getting_started/navigation.rst:70
msgid ""
"You can arrange the dockers in almost any permutation and combination "
"according to the needs of your workflow, and then save these arrangements as "
"Workspaces."
msgstr ""
"Els podreu organitzar en gairebé qualsevol permutació i combinació d'acord "
"amb les necessitats del vostre flux de treball i, després, desar aquests "
"ajustaments com a espais de treball."

#: ../../user_manual/getting_started/navigation.rst:72
msgid ""
"Dockers can be prevented from docking by pressing the :kbd:`Ctrl` key before "
"starting to drag the docker."
msgstr ""
"Als acobladors se'ls pot impedir acoblar-se prement la tecla :kbd:`Ctrl` "
"abans de començar a arrossegar l'acoblador."

#: ../../user_manual/getting_started/navigation.rst:75
msgid "Sliders"
msgstr "Controls lliscants"

#: ../../user_manual/getting_started/navigation.rst:76
msgid ""
"Krita uses these to control values like brush size, opacity, flow, Hue, "
"Saturation, etc... Below is an example of a Krita slider."
msgstr ""
"El Krita utilitza aquests valors de control com la mida del pinzell, "
"l'opacitat, el flux, la tonalitat, la saturació, etc... A continuació es "
"mostra un control lliscant del Krita."

#: ../../user_manual/getting_started/navigation.rst:79
msgid ".. image:: images/Krita_Opacity_Slider.png"
msgstr ".. image:: images/Krita_Opacity_Slider.png"

#: ../../user_manual/getting_started/navigation.rst:80
msgid ""
"The total range is represented from left to right and blue bar gives an "
"indication of where in the possible range the current value is. Clicking "
"anywhere, left or right, of that slider will change the current number to "
"something lower (to the left) or higher (to the right)."
msgstr ""
"L'interval total està representat d'esquerra a dreta i la barra blava indica "
"quins són els intervals possibles per al valor actual. En fer clic a "
"qualsevol lloc, a l'esquerra o a la dreta, aquest control lliscant canviarà "
"el número actual a quelcom més baix (a l'esquerra) o més alt (a la dreta)."

#: ../../user_manual/getting_started/navigation.rst:82
msgid ""
"To input a specific number, |mouseright| the slider. A number can now be "
"entered directly for even greater precision."
msgstr ""
"Per introduir un número específic, feu |mouseright| sobre el control "
"lliscant. Ja podreu introduir un número directament per obtenir una precisió "
"encara més gran."

#: ../../user_manual/getting_started/navigation.rst:84
msgid ""
"Pressing the :kbd:`Shift` key while dragging the slider changes the values "
"at a smaller increment, and pressing the :kbd:`Ctrl` key while dragging the "
"slider changes the value in whole numbers or multiples of 5."
msgstr ""
"Prement la tecla :kbd:`Majús.` mentre s'arrossega el control lliscant es "
"canviaran els valors en un increment més petit, i prement la tecla :kbd:"
"`Ctrl` mentre s'arrossega el control lliscant es canviaran els valors en "
"nombres sencers o múltiples de 5."

#: ../../user_manual/getting_started/navigation.rst:87
msgid "Toolbars"
msgstr "Barres d'eines"

#: ../../user_manual/getting_started/navigation.rst:89
msgid ".. image:: images/Krita_Toolbar.png"
msgstr ".. image:: images/Krita_Toolbar.png"

#: ../../user_manual/getting_started/navigation.rst:90
msgid ""
"Toolbars are where some of the important actions and menus are placed so "
"that they are readily and quickly available for the artist while painting."
msgstr ""
"Les barres d'eines és on es col·loquen algunes de les accions i menús "
"importants, de manera que estiguin disponibles amb facilitat per a l'artista "
"durant la pintura."

#: ../../user_manual/getting_started/navigation.rst:92
msgid ""
"You can learn more about the Krita Toolbars and how to configure them in "
"over in the :ref:`Toolbars section <configure_toolbars>` of the manual. "
"Putting these to effective use can really speed up the Artist's workflow, "
"especially for users of Tablet-Monitors and Tablet-PCs."
msgstr ""
"Podeu obtenir més informació sobre les barres d'eines del Krita i sobre com "
"configurar-les a la :ref:`secció Barres d'eines <configure_toolbars>` del "
"manual. Posar-les aquí pot accelerar realment el flux de treball de "
"l'artista, especialment per als usuaris de monitors i ordinadors de tauleta."

#: ../../user_manual/getting_started/navigation.rst:96
msgid "Workspace Chooser"
msgstr "Triar l'espai de treball"

#: ../../user_manual/getting_started/navigation.rst:98
msgid ""
"The button on the very right of the Toolbar is the workspace chooser. This "
"allows you to load and save common configurations of the user interface in "
"Krita. There are a few common workspaces that come with Krita."
msgstr ""
"El botó que es troba a la dreta de la Barra d'eines és el selector de "
"l'espai de treball. Aquest permet carregar i desar les configuracions "
"habituals de la interfície d'usuari en el Krita. Hi ha alguns espais de "
"treball habituals que ja vénen amb el Krita."

#: ../../user_manual/getting_started/navigation.rst:101
msgid ".. image:: images/workspace-chooser-button.svg"
msgstr ".. image:: images/workspace-chooser-button.svg"

#: ../../user_manual/getting_started/navigation.rst:108
msgid ""
":ref:`Pop-up Palette <pop-up_palette>` is a feature unique to Krita, "
"designed to increase the productivity of the artist. It is a circular menu "
"for quickly choosing brushes, foreground and background colors, recent "
"colors while painting. To access the palette you have to just |mouseright| "
"on the canvas. The palette will spawn at the position of the brush tip or "
"cursor."
msgstr ""
"La :ref:`Paleta emergent <pop-up_palette>` és una característica única del "
"Krita, dissenyada per fer augmentar la productivitat de l'artista. És un "
"menú circular per escollir ràpidament pinzells, colors de primer pla i de "
"fons, colors recents mentre es pinta. Per accedir a la paleta haureu de fer |"
"mouseright| sobre el llenç. La paleta apareixerà al lloc de la punta del "
"pinzell o del punter del ratolí."

#: ../../user_manual/getting_started/navigation.rst:110
msgid ""
"By tagging your brush presets you can add particular sets of brushes to this "
"palette. For example, if you add some inking brush presets to inking tag you "
"can change the tags to inking in the pop-up palette and you'll get all the "
"inking brushes in the palette."
msgstr ""
"Si etiqueteu els pinzells predefinits, podreu afegir conjunts de pinzells a "
"aquesta paleta. Per exemple, si afegiu alguns pinzells predefinits de tinta "
"a l'etiqueta tintes, podreu canviar les etiquetes per a tintes a la paleta "
"emergent i obtindreu tots els pinzells de tinta a la paleta."

#: ../../user_manual/getting_started/navigation.rst:112
msgid ""
"You can :ref:`tag <tag_management>` brush presets via the :ref:"
"`brush_preset_docker`, check out the :ref:`resource overview page "
"<resource_management>` to know more about tagging in general."
msgstr ""
"Podeu establir una :ref:`etiqueta <tag_management>` amb pinzells predefinits "
"mitjançant l':ref:`brush_preset_docker`, reviseu la :ref:`pàgina de resum "
"del recurs <resource_management>` per a saber més sobre l'etiquetatge en "
"general."

#: ../../user_manual/getting_started/navigation.rst:114
msgid ""
"If you call up the pop-up palette again, you can click the tag icon, and "
"select the tag. In fact, you can make multiple tags and switch between them. "
"When you need more than ten presets, go into :menuselection:`Settings --> "
"Configure Krita --> General --> Miscellaneous --> Number of Palette Presets` "
"and change the number of presets from 10 to something you feel comfortable."
msgstr ""
"Si torneu a fer servir la paleta emergent, podreu fer clic a la icona de "
"l'etiqueta i seleccionar l'etiqueta. De fet, podeu crear múltiples etiquetes "
"i alternar entre elles. Quan necessiteu més de deu configuracions "
"predefinides, aneu a :menuselection:`Arranjament --> Configura el Krita --> "
"General --> Miscel·lània --> Nombre de predefinits de la paleta` i canvieu "
"el nombre de predefinits des de 10 a quelcom que us faci sentir còmode."
