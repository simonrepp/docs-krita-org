# translation of docs_krita_org_reference_manual___filters___enhance.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters___enhance\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 10:29+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/filters/enhance.rst:1
msgid "Overview of the enhance filters."
msgstr ""

#: ../../reference_manual/filters/enhance.rst:10
#: ../../reference_manual/filters/enhance.rst:19
msgid "Sharpen"
msgstr "Zaostrenie"

#: ../../reference_manual/filters/enhance.rst:10
msgid "Filters"
msgstr ""

#: ../../reference_manual/filters/enhance.rst:15
msgid "Enhance"
msgstr "Rozšíriť"

#: ../../reference_manual/filters/enhance.rst:17
msgid ""
"These filters all focus on reducing the blur in the image by sharpening and "
"enhancing details and the edges. Following are various sharpen and enhance "
"filters in provided in Krita."
msgstr ""

#: ../../reference_manual/filters/enhance.rst:20
msgid "Mean Removal"
msgstr "Odstránenie stredu"

#: ../../reference_manual/filters/enhance.rst:21
msgid "Unsharp Mask"
msgstr "Maskovanie neostrosti"

#: ../../reference_manual/filters/enhance.rst:22
msgid "Gaussian Noise reduction"
msgstr "Gaussovo potlačenie šumu"

#: ../../reference_manual/filters/enhance.rst:23
msgid "Wavelet Noise Reducer"
msgstr "Vlnkové potlačenie šumu"
