# Translation of docs_krita_org_reference_manual___preferences___grid_settings.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___grid_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-21 03:28+0200\n"
"PO-Revision-Date: 2019-06-21 06:37+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../<generated>:1
msgid "Subdivision"
msgstr "Підпроміжки"

#: ../../reference_manual/preferences/grid_settings.rst:1
msgid "Grid settings in Krita."
msgstr "Параметри ґратки у Krita."

#: ../../reference_manual/preferences/grid_settings.rst:15
msgid "Grid Settings"
msgstr "Параметри ґратки"

#: ../../reference_manual/preferences/grid_settings.rst:19
msgid "Deprecated in 3.0, use the :ref:`grids_and_guides_docker` instead."
msgstr ""
"Вважається застарілим, починаючи з версії 3.0, користуйтеся інструментом :"
"ref:`grids_and_guides_docker`."

#: ../../reference_manual/preferences/grid_settings.rst:21
msgid "Use :menuselection:`Settings --> Configure Krita --> Grid` menu item."
msgstr ""
"Скористайтеся пунктом меню :menuselection:`Параметри --> Налаштувати Krita --"
"> Ґратка`."

#: ../../reference_manual/preferences/grid_settings.rst:24
msgid ".. image:: images/preferences/Krita_Preferences_Grid.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Grid.png"

#: ../../reference_manual/preferences/grid_settings.rst:25
msgid "Fine tune the settings of the grid-tool grid here."
msgstr "Тут можна скоригувати параметри інструмента ґратки."

#: ../../reference_manual/preferences/grid_settings.rst:28
msgid "Placement"
msgstr "Розташування"

#: ../../reference_manual/preferences/grid_settings.rst:30
msgid "The user can set various settings of the grid over here."
msgstr "Тут користувач може встановити різноманітні параметри ґратки."

#: ../../reference_manual/preferences/grid_settings.rst:32
msgid "Horizontal Spacing"
msgstr "Горизонтальний інтервал"

#: ../../reference_manual/preferences/grid_settings.rst:33
msgid ""
"The number in Krita units, the grid will be spaced in the horizontal "
"direction."
msgstr ""
"Число у одиницях виміру Krita, яке визначатиме інтервал між лініями ґратки у "
"горизонтальному напрямку."

#: ../../reference_manual/preferences/grid_settings.rst:35
msgid "Vertical Spacing"
msgstr "Вертикальний інтервал"

#: ../../reference_manual/preferences/grid_settings.rst:35
msgid ""
"The number in Krita units, the grid will be spaced in the vertical "
"direction. The images below will show the usage of these settings."
msgstr ""
"Число у одиницях виміру Krita, яке визначатиме інтервал між лініями ґратки у "
"вертикальному напрямку. На зображеннях, які наведено нижче, показано "
"використання цих параметрів."

#: ../../reference_manual/preferences/grid_settings.rst:37
msgid "X Offset"
msgstr "Зміщення за X"

#: ../../reference_manual/preferences/grid_settings.rst:38
msgid "The number to offset the grid in the X direction."
msgstr "Зсув ґратки у напрямку X."

#: ../../reference_manual/preferences/grid_settings.rst:40
msgid "Y Offset"
msgstr "Зміщення за Y"

#: ../../reference_manual/preferences/grid_settings.rst:40
msgid "The number to offset the grid in the Y direction."
msgstr "Зсув ґратки у напрямку Y."

#: ../../reference_manual/preferences/grid_settings.rst:42
msgid ""
"Some examples are shown below, look at the edge of the image to see the "
"offset."
msgstr "Деякі приклади показано нижче. Відступ наведено на краю зображення."

#: ../../reference_manual/preferences/grid_settings.rst:45
msgid "Subdivisions"
msgstr "Піделементи"

#: ../../reference_manual/preferences/grid_settings.rst:45
msgid ""
"Here the user can set the number of times the grid is subdivided. Some "
"examples are shown below."
msgstr ""
"Тут користувач може вказати кількість проміжних поділів ґратки. Декілька "
"прикладів наведено нижче."

#: ../../reference_manual/preferences/grid_settings.rst:48
msgid "Style"
msgstr "Стиль"

#: ../../reference_manual/preferences/grid_settings.rst:50
msgid "Main"
msgstr "Основний"

#: ../../reference_manual/preferences/grid_settings.rst:51
msgid ""
"The user can set how the main lines of the grid are shown. Options available "
"are Lines, Dashed Lines, Dots. The color also can be set here."
msgstr ""
"Користувач може вказати спосіб показу основних ліній ґратки. Передбачені "
"варіанти: суцільна лінія, штрихова лінія, пунктир. Також можна визначити "
"колір ліній."

#: ../../reference_manual/preferences/grid_settings.rst:53
msgid ""
"The user can set how the subdivision lines of the grid are shown. Options "
"available are Lines, Dashed Lines, Dots. The color also can be set here."
msgstr ""
"Користувач може вказати спосіб показу проміжних ліній ґратки. Передбачені "
"варіанти: суцільна лінія, штрихова лінія, пунктир. Також можна визначити "
"колір ліній."
