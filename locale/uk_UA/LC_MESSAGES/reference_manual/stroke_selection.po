# Translation of docs_krita_org_reference_manual___stroke_selection.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___stroke_selection\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:35+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/Stroke_Selection_4.png"
msgstr ".. image:: images/Stroke_Selection_4.png"

#: ../../reference_manual/stroke_selection.rst:1
msgid "How to use the stroke selection command in Krita."
msgstr "Як користуватися командою «Обвести позначене» у Krita."

#: ../../reference_manual/stroke_selection.rst:10
msgid "Selection"
msgstr "Позначення"

#: ../../reference_manual/stroke_selection.rst:10
msgid "Stroke"
msgstr "Мазок"

#: ../../reference_manual/stroke_selection.rst:15
msgid "Stroke Selection"
msgstr "Обведення позначеного"

#: ../../reference_manual/stroke_selection.rst:17
msgid ""
"Sometimes, you want to add an even border around a selection. Stroke "
"Selection allows you to do this. It's under :menuselection:`Edit --> Stroke "
"Selection`."
msgstr ""
"Іноді, виникає потреба у додаванні однорідної рамки навколо позначеної "
"ділянки. За допомогою інструмента обведення позначеного можна додати таку "
"рамку. Інструмент можна викликати за допомогою пункту меню :menuselection:"
"`Зміни --> Обвести позначене`."

#: ../../reference_manual/stroke_selection.rst:19
msgid "First make a selection and call up the menu:"
msgstr "Спочатку позначте щось, потім скористайтеся пунктом меню:"

#: ../../reference_manual/stroke_selection.rst:22
msgid ".. image:: images/Krita_stroke_selection_1.png"
msgstr ".. image:: images/Krita_stroke_selection_1.png"

#: ../../reference_manual/stroke_selection.rst:23
msgid ""
"The main options are about using the current brush, or lining the selection "
"with an even line. You can use the current foreground color, the background "
"color or a custom color."
msgstr ""
"Основні параметри стосуються використання поточного пензля або заповнення "
"позначеної однорідною лінією ділянки. Ви можете скористатися поточним "
"кольором переднього плану, кольором тла або нетиповим кольором."

#: ../../reference_manual/stroke_selection.rst:25
msgid "Using the current brush allows you to use textured brushes:"
msgstr ""
"Використання поточного пензля надасть вам змогу скористатися текстурованими "
"пензлями:"

#: ../../reference_manual/stroke_selection.rst:28
msgid ".. image:: images/Stroke_selection_2.png"
msgstr ".. image:: images/Stroke_selection_2.png"

#: ../../reference_manual/stroke_selection.rst:29
msgid ""
"Lining the selection also allows you to set the background color, on top of "
"the line width in pixels or inches:"
msgstr ""
"Для заповнення позначеної ділянки також можна встановити колір тла, а також "
"товщину лінії у пікселях або дюймах."

#: ../../reference_manual/stroke_selection.rst:32
msgid ".. image:: images/Krita_stroke_selection_3.png"
msgstr ".. image:: images/Krita_stroke_selection_3.png"

#: ../../reference_manual/stroke_selection.rst:33
msgid "This creates nice silhouettes:"
msgstr "Буде створено чудові силуети:"
