msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-16 17:05\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___welcome_screen.pot\n"

#: ../../reference_manual/welcome_screen.rst:None
msgid ".. image:: images/welcome_screen.png"
msgstr ""

#: ../../reference_manual/welcome_screen.rst:1
msgid "The welcome screen in Krita."
msgstr ""

#: ../../reference_manual/welcome_screen.rst:10
#: ../../reference_manual/welcome_screen.rst:14
msgid "Welcome Screen"
msgstr ""

#: ../../reference_manual/welcome_screen.rst:16
msgid ""
"When you open Krita, starting from version 4.1.3, you will be greeted by a "
"welcome screen. This screen makes it easy for you to get started with Krita, "
"as it provides a collection of shortcuts for the most common tasks that you "
"will probably be doing when you open Krita."
msgstr ""

#: ../../reference_manual/welcome_screen.rst:23
msgid "The screen is divided into 4 sections:"
msgstr ""

#: ../../reference_manual/welcome_screen.rst:25
msgid ""
"The :guilabel:`Start` section there are links to create new document as well "
"to open an existing document."
msgstr ""

#: ../../reference_manual/welcome_screen.rst:28
msgid ""
"The :guilabel:`Recent Documents` section has a list of recently opened "
"documents from your previous sessions in Krita."
msgstr ""

#: ../../reference_manual/welcome_screen.rst:31
msgid ""
"The :guilabel:`Community` section provides some links to get help, "
"Supporting development of Krita, Source code of Krita and to links to "
"interact with our user community."
msgstr ""

#: ../../reference_manual/welcome_screen.rst:35
msgid ""
"The :guilabel:`News` section, which is disabled by default, when enabled "
"provides you with latest news feeds fetched from Krita website, this will "
"help you stay up to date with the release news and other events happening in "
"our community."
msgstr ""

#: ../../reference_manual/welcome_screen.rst:39
msgid ""
"Other than the above sections the welcome screen also acts as a drop area "
"for opening any document. You just have to drag and drop a Krita document or "
"any supported image files on the empty area around the sections to open it "
"in Krita."
msgstr ""
