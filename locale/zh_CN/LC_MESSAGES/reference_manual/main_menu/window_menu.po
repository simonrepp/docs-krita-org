msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___main_menu___window_menu.pot\n"

#: ../../<generated>:1
msgid "List of open documents."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:1
msgid "The window menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:11
msgid "Window"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:11
msgid "View"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:16
msgid "Window Menu"
msgstr "窗口菜单"

#: ../../reference_manual/main_menu/window_menu.rst:18
msgid "A menu completely dedicated to window management in Krita."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:20
msgid "New Window"
msgstr "新窗口"

#: ../../reference_manual/main_menu/window_menu.rst:21
msgid "Creates a new window for Krita. Useful with multiple screens."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:22
msgid "New View"
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:23
msgid ""
"Make a new view of the given document. You can have different zoom or "
"rotation on these."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:24
msgid "Workspace"
msgstr "工作空间"

#: ../../reference_manual/main_menu/window_menu.rst:25
msgid "A convenient access panel to the :ref:`resource_workspaces`."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:26
msgid "Close"
msgstr "关闭"

#: ../../reference_manual/main_menu/window_menu.rst:27
msgid "Close the current view."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:28
msgid "Close All"
msgstr "关闭全部"

#: ../../reference_manual/main_menu/window_menu.rst:29
msgid "Close all documents."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:30
msgid "Tile"
msgstr "平铺"

#: ../../reference_manual/main_menu/window_menu.rst:31
msgid "Tiles all open documents into a little sub-window."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:32
msgid "Cascade"
msgstr "级联"

#: ../../reference_manual/main_menu/window_menu.rst:33
msgid "Cascades the sub-windows."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:34
msgid "Next"
msgstr "下次执行"

#: ../../reference_manual/main_menu/window_menu.rst:35
msgid "Selects the next view."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:36
msgid "Previous"
msgstr "上一首"

#: ../../reference_manual/main_menu/window_menu.rst:37
msgid "Selects the previous view."
msgstr ""

#: ../../reference_manual/main_menu/window_menu.rst:39
msgid "Use this to switch between documents."
msgstr ""
